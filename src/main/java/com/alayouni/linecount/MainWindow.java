package com.alayouni.linecount;

import com.google.common.base.Splitter;
import org.apache.commons.lang.exception.ExceptionUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: alayouni
 */
public class MainWindow extends JFrame{

	private JTextField pathField;

	private JTextField extensionsField;

	private JTextArea outputArea;

	private JButton countButton;

	public MainWindow() {
		super();

		JPanel contentPane = new JPanel(new BorderLayout(0, 5)){
			@Override
			public Insets getInsets(){
				return new Insets(20, 20, 20, 20);
			}
		};
		JPanel inputPanel = createInputPanel();
		contentPane.add(inputPanel, BorderLayout.NORTH);

		outputArea = new JTextArea();
		outputArea.setEditable(false);
		JScrollPane scrollPane = new JScrollPane(outputArea);
		contentPane.add(scrollPane, BorderLayout.CENTER);

		setContentPane(contentPane);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(1000, 800);
		setTitle("Line Counter");
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int x = (screenSize.width - getSize().width) / 2,
				y = (screenSize.height - getSize().height) / 2;
		setLocation(x, y);
	}

	private JPanel createInputPanel() {
		JPanel inputPanel = new JPanel(new BorderLayout(5, 0));

		final int lineCount = 2;
		JPanel labelsPanel = new JPanel(new GridLayout(lineCount, 1));

		JLabel pathLabel = new JLabel("Root Path:");
		labelsPanel.add(pathLabel);

		JLabel extensionsLabel = new JLabel("Extensions (without dots, comma separated):");
		labelsPanel.add(extensionsLabel);

		inputPanel.add(labelsPanel, BorderLayout.WEST);

		JPanel textFieldsPanel = new JPanel(new GridLayout(lineCount, 1));

		pathField = new JTextField();
		textFieldsPanel.add(pathField);

		extensionsField = new JTextField();
		textFieldsPanel.add(extensionsField);

		inputPanel.add(textFieldsPanel, BorderLayout.CENTER);

		inputPanel.add(getCountButton(), BorderLayout.EAST);

		return inputPanel;
	}

	private JButton getCountButton() {
		if(countButton == null) {
			countButton = new JButton("Count");
			countButton.setForeground(Color.red);
			countButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					countButton.setEnabled(false);
					new Thread(new Runnable() {
						@Override
						public void run() {
							countLines();
						}
					}).start();
				}
			});
		}
		return countButton;
	}

	private void countLines() {
		String rootPath = pathField.getText().trim();
		if(!rootPath.isEmpty()) {
			String extensionsStr = extensionsField.getText().trim();
			if(!extensionsStr.isEmpty()) {
				Iterable<String> extensions = Splitter.on(",").trimResults().split(extensionsStr);
				List<ExtensionDescription> extensionDescriptions = new ArrayList<ExtensionDescription>();
				ExtensionDescription extensionDescription;
				File rootDirectory = new File(rootPath);
				for(String extension : extensions) {
					extensionDescription = new ExtensionDescription(extension);
					updateExtensionDescriptionUnderDirectory(extensionDescription, rootDirectory);
					extensionDescriptions.add(extensionDescription);

					log(extensionDescription.toString());
					log("\n___________________________________________________________\n\n");
				}

				int totalFileCount = 0,
						totalLineCount = 0;
				for(ExtensionDescription description : extensionDescriptions) {
					log(description.toString());
					totalFileCount += description.totalFileCount;
					totalLineCount += description.totalLineCount;
				}

				log(String.format("\n[Total File Count: %d]        [Total Line Count: %d]", totalFileCount, totalLineCount));
				log("\n\n DONE!\n\n");
			}
		}
		countButton.setEnabled(true);
	}

	private void updateExtensionDescriptionUnderDirectory(ExtensionDescription extensionDescription, File directory) {
		File[] files = directory.listFiles();
		for(File file : files) {
			if(file.isDirectory()) {
				updateExtensionDescriptionUnderDirectory(extensionDescription, file);
			} else if(file.getName().endsWith("." + extensionDescription.extension)){
				extensionDescription.totalFileCount ++;
				int lineCount = countFileLines(file);
				extensionDescription.totalLineCount += lineCount;
				log(String.format("%s: %d lines", file.getAbsolutePath(), lineCount));
			}
		}
	}

	private int countFileLines(File file) {
		try {
			BufferedReader reader = reader = new BufferedReader(new FileReader(file));
			int lines = 0;
			while (reader.readLine() != null) lines++;
			reader.close();
			return lines;
		} catch (java.io.IOException e) {
			log(ExceptionUtils.getFullStackTrace(e));
		}
		return -1;
	}


	private class ExtensionDescription
	{
		private int totalFileCount = 0;

		private int totalLineCount = 0;

		private final String extension;

		public ExtensionDescription(String extension) {
			this.extension = extension;
		}

		@Override
		public String toString() {
			return String.format(".%s files => [total file count = %d]    [total line count = %d]", extension, totalFileCount, totalLineCount);
		}
	}

	public static void main(String[] args) {
		new MainWindow().setVisible(true);
	}

	private void log(final String line) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				outputArea.append(String.format("%s\n", line));
			}
		});
	}
}
